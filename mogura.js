$(window).load(function() {
  fittingImages;
});

fittingImages = function(){
  $(".fitting_img").each(function() {
    var e, t, n, r, i, s, o, u;
    return r = $(this).parent().innerHeight(), i = $(this).parent().innerWidth(), n = i / r, o = $(this).height(), u = $(this).width(), s = u / o, s > n ? (t = u * r / o, $(this).css({position: "absolute", left: "50%", top: "50%", height: r, width: t, marginLeft: -t / 2, marginTop: -r / 2})) : (e = o * i / u, $(this).css({position: "absolute", left: "50%", top: "50%", width: i, height: e, marginLeft: -i / 2, marginTop: -e / 2}))
  });
}
